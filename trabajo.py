import json
from hashlib import sha1
import time
from conexion import ApiConexion
import pandas as pd

class Pais:

    datapaises= ""
    dataregiones= ""

    

    def __init__(self):
        """
        Contructor para instanciar la clase ApiConexion
        """
        self.datapaises= ApiConexion()
        self.dataregiones=ApiConexion()
        self.conexion = ApiConexion()
        

    def regiones(self):
        """Toma las regiones exitentes del api https://restcountries-v1.p.rapidapi.com/all

        Devuelve una lista con las regiones existentes

        Consultando el metodo ApiRegiones se devuelve una lista con toda la informacion de la api
        Se recorre la lista para tomar todas las regiones existentes
        Se eliminan las regiones repetidas

        """
        data=[]
        data= self.datapaises.ApiRegiones()
        regionesR=[]
        regiones1=[]
        for i in data:
            regionesR.append(i['region'])
        for i in regionesR:
            if i not in regiones1:
                regiones1.append(i)
        regiones1.pop(6)
        return regiones1

    def paiseseIdiomasporRegion(self):
        """
        Toma la información solicitada de los puntos 2,3,4 de Exercise-Level 1

        Devuelve una lista con la información solicita de: Region, Pais, Idioma y Time

        Se consulta el metodo ApiPaisesporRegion el cual devulve la informacion de los paises realizando la entradas de las regiones

        """

        regiones=[]
        idiomas=[]
        paises=[]
        regiones= self.regiones()
        for i in regiones:
            data2 = self.dataregiones.ApiPaisesporRegion(i)
            for j in data2:
                idiomas.append({
                'idiomas': (j['languages'][0]['name'])})
                s = json.dumps(idiomas).encode('utf-8')
                tiempo= time.time()
                paises.append({
                'Region': i,  
                'pais':j['name'],
                'idiomas': sha1(s).hexdigest(),
                'Time': round((time.time() - tiempo)*1000, 2 )
                })
        return paises

    def tiempos (self):
        """
        Se usan funciones de pandas para calcular tiempos de ejecucion de las filas

        Devulvel una cadena describiendo los resultados de las variables de tiempos    
        """
        paiseseIdiomasporRegion=[]
        paiseseIdiomasporRegion = self.paiseseIdiomasporRegion()
        df=pd.DataFrame(paiseseIdiomasporRegion)
        Min_time= df['Time'].min()
        Max_time= df['Time'].max()
        prom_time= df['Time'].mean()
        total_time= df['Time'].sum()
        return "Tiempo min ", Min_time, "tiempo max ", Max_time, "tiempo promedio",  prom_time, "tiempo total", total_time
    
    def guardarsqlitePaises(self):
        """
        Se realiza la inserción de Region, Pais, Idioma, Time

        
        """
        cnx= self.conexion.ConexionSqlite()
        paiseseIdiomasporRegion=[]
        paiseseIdiomasporRegion = self.paiseseIdiomasporRegion()
        for i in paiseseIdiomasporRegion:
            sql = ''' INSERT INTO Paises(region, pais,lenguaje,time)
              VALUES(?,?,?,?) '''
            cur = cnx.cursor()
            
            array= (i['Region'],i['pais'],i['idiomas'],i['Time'])
            
            cur.execute(sql, array)
            cnx.commit() 
        



            


